#include "dixon2.h"
#include <string.h>

extern int id_mod2[SIZE_FACTOR_BASE][SIZE_FACTOR_BASE];
extern int grid_mod2[SIZE_FACTOR_BASE][SIZE_FACTOR_BASE];
extern int grid[SIZE_FACTOR_BASE][SIZE_FACTOR_BASE];
extern int pexp[SIZE_FACTOR_BASE];

int main(int argc, char* argv[])
{
	memset(id_mod2, 0, sizeof(id_mod2));
	memset(grid_mod2, 0, sizeof(grid_mod2));
	memset(grid, 0, sizeof(grid));
	memset(pexp, 0, sizeof(pexp));

	sieve_era();

	vector<ll> out;
	factor_dixon(554346, out);

	int sz=out.size();
	sort(out.begin(), out.end());
	for (int i=0; i<sz; i++) {
		if(i) cout << "x";
		cout << out[i];
	}
	if(sz==1) cout << " (PRIME)";
	else cout << " (COMPOSITE)";
	cout << endl;

	return 0;
}