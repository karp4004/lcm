#include "dixon2.h"

#define MIN_B_QS 20000LL
#define MAX_ITER_QS 20000LL
#define MUL_FACTOR 1
#define EPS_MAT 100
#define MAX_INT 100000001

int id_mod2[SIZE_FACTOR_BASE][SIZE_FACTOR_BASE];
int grid_mod2[SIZE_FACTOR_BASE][SIZE_FACTOR_BASE];
int grid[SIZE_FACTOR_BASE][SIZE_FACTOR_BASE];
int pexp[SIZE_FACTOR_BASE];

vector<ll> p;
bool is_prime[MAX_INT];
int npfact, nprime;

ll get_B(ll n) { return ll(MUL_FACTOR * sqrt( exp( sqrt( log(n) * log( log(n) ) ) ) ) ) + 1; }

void sieve_era() {
	for (ll i=0; i<MAX_INT; i++) is_prime[i]=true;
	is_prime[0]=is_prime[1]=false;
	for (ll i=2; i*i<MAX_INT; i++)
		if(is_prime[i])
			for (ll j=i; j*i<MAX_INT; j++)
				is_prime[j*i]=false;
	for (ll i=0; i<MAX_INT; i++)
		if(is_prime[i])
			p.push_back(i);
	nprime=p.size();
}

ll mulmod(ll a, ll b, ll c) {
	ll x=0, y=a%c;
	while(b>0) {
		if(b%2) x=(x+y)%c;
		y=(y*2)%c;
		b/=2;
	}
	return (x%c);
}

bool isSmooth(ll n, vector<ll> & pp) {
	int sz=pp.size();
	for (int i=0; i<sz; i++) {
		ll d=pp[i]; if(d>n) break;
		while(1) {
			if(n%d) break;
			n/=d;
		}
	}
	return (n==1);
}
void procSmooth(ll n, vector<ll> & pp, int idx) {
	int sz=pp.size();
	for (int i=0; i<sz; i++) {
		ll d=pp[i];
		int cur=0;
		while(1) {
			if(n%2) break;
			n/=d; cur++;
		}
		grid[idx][i]=cur;
		grid_mod2[idx][i]=cur%2;
	}
}

void gauss_elim_mod2(int grid_mod2[SIZE_FACTOR_BASE][SIZE_FACTOR_BASE], int nrow, int ncol) {
	for (int i=0; i<nrow; i++) {
		int i_max=i;
		for (int ii=i; ii<nrow; ii++)
			if(grid_mod2[ii][i]==1) { i_max=ii; break; }
			if(i_max!=i) {
				for (int j=0; j<ncol; j++) swap(grid_mod2[i][j],grid_mod2[i_max][j]);
				for (int j=0; j<nrow; j++) swap(id_mod2[i][j],id_mod2[i_max][j]);
			}
			for (int ii=i+1; ii<nrow; ii++)
				if(grid_mod2[ii][i]) {
					for (int jj=i; jj<ncol; jj++) grid_mod2[ii][jj]^=grid_mod2[i][jj];
					for (int jj=0; jj<nrow; jj++) id_mod2[ii][jj]^=id_mod2[i][jj];
				}
	}
}

ll mod(ll a, ll b, ll c) {
	ll r=1;
	while(b) {
		if(b%2) r=(r*a)%c;
		a=(a*a)%c;
		b/=2;
	}
	return r;
}

ll gcd(ll a, ll b) {
	if(!b) return a;
	else return gcd(b, a%b);
}

bool factorable_dixon(ll n, ll &xx, ll &yy) {
	ll B = min(n, max(MIN_B_QS, get_B(n)));
	vector<ll> v;
	for (int i=0; i<=B; i++)
		if(is_prime[i]) v.push_back(i);
	int sz=v.size(), nrow=ll(sz+EPS_MAT), ncol=sz;
	vector<ll> zo, zsq;
	ll z=ll(sqrt(n))+1;
	int idx=0, iter=0;
	while(1) {
		if(idx>=nrow) break;
		if(iter>=MAX_ITER_QS) { nrow=idx; break; }
		ll cur=(z*z)%n;
		if(isSmooth(cur,v)) { procSmooth(cur,v,idx); zo.push_back(z); zsq.push_back(cur); idx++; }
		z++; iter++;
	}
	for (int i=0; i<nrow; i++)
		for (int j=0; j<nrow; j++) {
			if(i==j) id_mod2[i][j]=1;
			else id_mod2[i][j]=0;
		}
		gauss_elim_mod2(grid_mod2,nrow,ncol);
		for (int i=0; i<nrow; i++) {
			bool ok=true;
			for (int j=0; j<ncol; j++) if(grid_mod2[i][j]) { ok=false; break; }
			if(ok) {
				ll x=1,y=1;
				for (int j=0; j<ncol; j++) pexp[j]=0;
				for (int j=0; j<nrow; j++)
					if(id_mod2[i][j]) {
						x=mulmod(x,zo[j],n);
						for (int k=0; k<ncol; k++) pexp[k]+=grid[j][k];
					}
					for (int j=0; j<ncol; j++) { pexp[j]/=2; y*=mod(v[j],pexp[j],n); }
					ll g=gcd((x+y)%n, n);
					if(1<g && g<n) { xx=g; yy=n/g; return true; }
			}
		}
		return false;
}

void factor_dixon(ll n, vector<ll>& factor) {
	queue<ll> q; q.push(n);
	ll x,y;
	while(!q.empty()) {
		ll cur=q.front(); q.pop();
		if(factorable_dixon(cur,x,y)) { q.push(x); q.push(y); }else factor.push_back(cur);
	}
}