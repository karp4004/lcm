#ifndef dix
#define dix

#include <vector>
#include <queue>
#include <iostream>

using namespace std;

#define SIZE_FACTOR_BASE 5000

typedef long long ll;

void factor_dixon(ll n, vector<ll>& factor);
void sieve_era();

#endif dix