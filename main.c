#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <unistd.h>
#include "dixon.h"

/* 
 * main 
 * dixon's algorithm
 * implementation by jeff hamblin
 */

int main(int argc, char **argv)
{
	int i, j, prime_count = 0, *p;
	mpz_t composite, sqrt_composite, scratch, factor, remaining, lower_z;
	int smooth_bound, pi_bound; int *primes; 
	int vector_index = 0;
	entry *vectors; 
	entry temp_vector;
	relation *relations;

	if (argc != 3)
	{
		printf("usage: %s composite prime-bound\n", argv[0]);
		exit(-1);
	}

	mpz_init(composite);
	mpz_init(sqrt_composite);
	mpz_init(scratch);
	mpz_init(factor);
	mpz_init(remaining);
	mpz_init(lower_z);

	/* initialize values of composite to factor, smoothness parameter */

	mpz_set_str(composite, argv[1], 10);
	mpz_set(remaining, composite);
	mpz_sqrt(sqrt_composite, composite);
	smooth_bound = atoi(argv[2]);
	p = &prime_count;
	p = 0;

	/* make sure composite is positive */
	if (mpz_cmp_si(composite, 0) < 0)
	{
		printf("\tfactor: -1\n");
		mpz_abs(composite, composite);
	}

	/* first, get rid of powers of 2 in composite */

	while (mpz_mod_ui(scratch, composite, 2) == 0)
	{
		prime_count ++;
		printf("\tfactor: 2\n");
		mpz_div_ui(composite, composite, 2);
	}

	/* check to see if composite is completely factored */

	if (done_factoring(composite, &prime_count))
	{
		exit(prime_count);
	}

	/* find primes less than smoothness bound with sieve of erasthones */

	primes = malloc(sizeof(int) * smooth_bound);
	pi_bound = sieve(smooth_bound, primes);
	
	/* useless little title */
	printf("---\nPerforming Dixon's Algorithm\n---\n");
	printf("found %d primes below %d\n", pi_bound, smooth_bound);
	mpz_out_str(stdout, 10, composite);
	printf(" = \n");

	/* set the size of the exponent vectors to be the number of primes in factor
	 * base, and initialize them all to 0.  initialize the mpz term, also. */

	temp_vector.exp = malloc(sizeof(int) * pi_bound);
	memset(temp_vector.exp, 0, sizeof(int) * pi_bound);
	mpz_init(temp_vector.z);
	mpz_init(temp_vector.w);

	/* we want to get additional relations, so we gather pi_bound + RELATIONS 
	 * decompositions.  vector is the collection of these exponent vectors. */

	vectors = malloc(sizeof(entry) * (pi_bound + RELATIONS));
	for (i = 0; i < pi_bound + RELATIONS; i ++)
	{
		vectors[i].exp = malloc(sizeof(int) * pi_bound);
		memset(vectors[i].exp, 0, sizeof(int) * pi_bound);
		mpz_init(vectors[i].z);
		mpz_init(vectors[i].w);
	}


	/* now collect pi_bound + RELATIONS different z values that satisfy w = z^2 mod n. 
	 * w is smooth_bound smooth; that is, that w is factorable over 
	 * primes < smooth_bound */ 

	mpz_set(lower_z, sqrt_composite);
	for (i = 0; i < pi_bound + RELATIONS; i ++)
	{
		get_z(composite, primes, smooth_bound, &vectors[i], lower_z); 
		printf("Found %d (of %d) z values.\n", i, pi_bound + RELATIONS);

		/* now display the z values and the exponent vectors of w = z^2 mod n */
		
		/*
		printf("\tz^2 = ");
		mpz_out_str(stdout, 10, vectors[i].z);
		printf("\n\tz^2 mod n = ");
		mpz_out_str(stdout, 10, vectors[i].w);

		printf(" = ");
		for (j = 0; j < pi_bound; j ++)
		{
			printf("%d ", vectors[i].exp[j]);
		}

		printf("\n\n");
		*/

	}

	/* we are going to gather relation number of linearly dependent vectors that
	 * have even exponents in their decomposition over the factor base.  so i
	 * have to initialize the relations array */

	relations = malloc(sizeof(relation) * RELATIONS);
	for (i = 0; i < RELATIONS; i ++)
	{
		mpz_init(relations[i].w_product);
		mpz_set_ui(relations[i].w_product, 1);
		mpz_init(relations[i].z_product);
		mpz_set_ui(relations[i].z_product, 1);
	}


	/* now perform gaussian elimination to get a subset of w's such that the
	 * exponents in the product of w's are all even, that is, the elements of
	 * the subset multiplied together yield a perfect square.  this perfect
	 * square is congruent to the corresponding z^2 product for those elements.
	 * now composite | w'^2 - z'^2, and we hopefully have a nontrivial factor
	 */

	gaussian_elim(vectors, relations, pi_bound, composite);

	/* now examine the RELATIONS relations and see if any of them give
	 * nontrivial factors of composite.  the w and z terms in these rows
	 * _should_ be perfect squares, if all has gone correctly.  w = z mod n, so
	 * check the gcd of (sqrt(w) - sqrt(z), n).  */

	mpz_set(remaining, composite);
	for (i = 0; i < RELATIONS; i ++)
	{
		if (examine_relations(&relations[i], composite, factor))
		{
			mpz_mod(scratch, composite, factor);
			if (mpz_cmp_ui(scratch, 0) == 0)
			{
				prime_count ++;
				printf("\tfactor: ");
				mpz_out_str(stdout, 10, factor);
				printf("\n");

				//mpz_div(remaining, remaining, factor);
				mpz_div(composite, composite, factor);

				if (done_factoring(composite, &prime_count))
				{
					exit(prime_count);
				}
			}
		}
	}
	printf("Unfactored: ");
	mpz_out_str(stdout, 10, remaining);
	printf("\n");
	return 1;
}
